<?php
// curl -X POST -d '{"object_kind":"push","before":"34598af362c7eed631df332cf3ca7adea6d659ad","after":"f137b68735f3dcadb9b95ba816df7e7f6a032dd4","ref":"refs/heads/master","checkout_sha":"f137b68735f3dcadb9b95ba816df7e7f6a032dd4","message":null,"user_id":362117,"user_name":"Skruppy","user_email":"skruppy@onmars.eu","project_id":720596,"repository":{"name":"test","url":"git@gitlab.com:Skrupellos/test.git","description":"","homepage":"https://gitlab.com/Skrupellos/test","git_http_url":"https://gitlab.com/Skrupellos/test.git","git_ssh_url":"git@gitlab.com:Skrupellos/test.git","visibility_level":20},"commits":[{"id":"f137b68735f3dcadb9b95ba816df7e7f6a032dd4","message":"changed readme","timestamp":"2015-12-31T00:54:46+00:00","url":"https://gitlab.com/Skrupellos/test/commit/f137b68735f3dcadb9b95ba816df7e7f6a032dd4","author":{"name":"Skruppy","email":"skruppy@onmars.eu"},"added":[],"modified":["README.md"],"removed":[]},{"id":"34598af362c7eed631df332cf3ca7adea6d659ad","message":"added readme","timestamp":"2015-12-31T00:54:22+00:00","url":"https://gitlab.com/Skrupellos/test/commit/34598af362c7eed631df332cf3ca7adea6d659ad","author":{"name":"Skruppy","email":"skruppy@onmars.eu"},"added":["README.md"],"modified":[],"removed":[]}],"total_commits_count":2}' https://onmars.eu/php/gitlabhook.php

function startsWith($haystack, $needle) {
	return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function endsWith($haystack, $needle) {
	return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

function template($tpl, $vars, $open = '{{', $close = '}}') {
	$parts = explode($open, $tpl);
	
	// Skip the first, it can never contain a variable
	for($i = 1; $i < count($parts); $i++) {
		$part = $parts[$i];
		
		// Check for escaped open tags (in this case: double open tags)
		if(startsWith($part, $open)) {
			continue;
		}
		
		// Replace variable
		$partyParts = explode($close, $part, 2);
		if(count($partyParts) != 2) {
			throw Exception("{$i}th variable is missing closing tag (\"{$close}\")");
		}
		
		if(!isset($vars[$partyParts[0]])) {
			throw Exception("{$i}th variable \"{$partyParts[0]}\" is not defined.");
		}
		
		// Use only the first line of text, this sanitizes the variable
		$parts[$i] = (explode("\n", $vars[$partyParts[0]], 2)[0]).$partyParts[1];
	}
	
	return implode('', $parts);
}

function logger($msg) {
	// '/srv/http/start/php/gitlabhook.log'
	file_put_contents('/tmp/gitlabhook.log', $msg, FILE_APPEND);
}

function send($msg) {
	$recipient = "#gaf-alarm";
	//file_put_contents('/run/sopel/pipe', "{$recipient} {$msg}\n");
}

function sendTpl($msg, $vars) {
	$msg = template($msg, $vars);
	
	logger($msg);
	send($msg);
}

$input = file_get_contents("php://input");
logger($input);

$json = json_decode($input);

if(!is_object($json)) {
	die('No valid JSON');
}

$tpls = [
	"push_summary" => "{{baseUrl}}/\x0306\x02{{group}}\x0F/\x0306{{repo}}\x0F/commits/\x0306{{branch}}\x0F: {{user_name}} pushed \x02{{total_commits_count}}\x0F commits",
	//"push_summary" => "[\x0306\x02{{group}}\x0F/\x0306{{repo}}\x0F] {{user_name}} pushed \x02{{total_commits_count}}\x0F commits to \x0306{{dst}}\x0F ({{homepage}})",
	"push_commit"  => " -> {{message}} ({{url}})",
];

// https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/web_hooks/web_hooks.md
if($json->object_kind == "push") {
	// "object_kind"         : "push",
	// "before"              : "95790bf891e76fee5e1747ab589903a6a1f80f22",
	// "after"               : "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
	// "ref"                 : "refs/heads/master",
	// "user_id"             : 4,
	// "user_name"           : "John Smith",
	// "user_email"          : "john@example.com",
	// "project_id"          : 15,
	// "total_commits_count" : 4,
	// "repository"          : {
	//   "name"             : "Diaspora",
	//   "url"              : "git@example.com:mike/diasporadiaspora.git",
	//   "description"      : "",
	//   "homepage"         : "http://example.com/mike/diaspora",
	//   "git_http_url"     : "http://example.com/mike/diaspora.git",
	//   "git_ssh_url"      : "git@example.com:mike/diaspora.git",
	//   "visibility_level" : 0,
	// },
	// "commits"             : {
	//   ...
	// },
	
	$urlParts = array_slice(explode("/", $json->repository->homepage), -2, 2);
	
	$vars = [
		"before"              => $json->before,
		"after"               => $json->after,
		"ref"                 => $json->ref,
		"user_id"             => $json->user_id,
		"user_name"           => $json->user_name,
		"user_email"          => $json->user_email,
		"project_id"          => $json->project_id,
		"name"                => $json->repository->name,
		"url"                 => $json->repository->url,
		"description"         => $json->repository->description,
		"homepage"            => $json->repository->homepage,
		"git_http_url"        => $json->repository->git_http_url,
		"git_ssh_url"         => $json->repository->git_ssh_url,
		"visibility_level"    => $json->repository->visibility_level,
		"total_commits_count" => $json->total_commits_count,
		"branch"              => str_replace("refs/heads/", "", $json->ref),
		"groupUrl"            => $parts[0],
		"projectUrl"          => $parts[1],
		"baseUrl"             => implode("/", array_slice(explode("/", $json->repository->homepage), 0, -2)),
	];
	
	
	sendTpl($tpls["push_summary"], $vars);
	
	foreach(array_slice($json->commits, 0, 3) as $commit) {
		// "id"        : "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
		// "message"   : "fixed readme",
		// "timestamp" : "2012-01-03T23:36:29+02:00",
		// "url"       : "http://example.com/mike/diaspora/commit/da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
		// "added"     : ["CHANGELOG"],
		// "modified"  : ["app/controller/application.rb"],
		// "removed"   : [],
		// "author"    : {
		//   "name"  : "GitLab dev user",
		//   "email" : "gitlabdev@dv6700.(none)"
		// },
		
		$commitVars = $vars;
		$commitVars["commit_id"]           = $commit->id;
		$commitVars["commit_message"]      = $commit->message;
		$commitVars["commit_timestamp"]    = $commit->timestamp;
		$commitVars["commit_url"]          = $commit->url;
		$commitVars["commit_author_name"]  = $commit->author->name;
		$commitVars["commit_author_email"] = $commit->author->email;
		
		sendTpl($tpls["push_commit"], $commitVars);
	}
}
